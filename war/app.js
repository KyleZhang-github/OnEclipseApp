/**
 * 
 */


Ext.define({
	name	:'Myapp',
	launch	:function(){
		Ext.widget({
			renderTo	:	Ext.getBody(),
			xtype		:	'grid',
			title		:	'張凱のGridテストソース！',
			width		:	650,
			height		:	300,
			plugins		:	'rowediting',
			store		:	{
				fields	:	['name','age','votes','credits'],
				data	:	[
				    	 	 ['zhangkai01',35,10,427],
				    	 	 ['zhangkai02',27,20,34]
				    	 	]
			},
			columns		:	{
				defaults	:	{
					editor	:	'numberfield',
					width	:	120
				},
				items	: 	[
				     	  	 {tex: 'Name',	dataIndex: 'name', flex: 1, editor: 'textfield'},
				     	  	 {text: 'Age', dataIndex: 'age'},
				     	  	 {text: 'Votes', dataIndex: 'votes'},
				     	  	 {text: 'Credits', dataIndex: 'credits'}
				     	  	 ]
			}
			
		});
	}
});